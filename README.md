# PlatformBuilders #

This is a project asked by PlatformBuilders as part of their hiring process. The goal is to develop a REST project for Client management, with GET, POST, DELETE, PATCH and PUT methods. 

### Stack ###

* Java 14
* Spring-Boot 2.3.4.RELEASE
* Junit 5
* Mockito 3.3.3
* MapStruct 1.3.1.Final
* Hibernate Validator 6.1.5.Final
* DockerCompose 1.26.2
* MySQL 8


### How do I get set up? ###

* To run the project you'll need to run the docker-compose first, so the mysql can be available. So, type on the project root directory the following command:

~~~~
docker-compose up -d
~~~~

* Then you can run the project typing the following command:

~~~~
./gradlew build bootRun
~~~~

* Now the project is running! There is a postman collection on the project root folder. Have fun!

### Main guidelines ###

* All business rules 100% tested by unit tests
* REST taken seriously
* Clean code
* SOLID