package br.com.platformbuilders.client.config;

import br.com.platformbuilders.client.exception.ClientNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(ClientNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected ResponseError handleClientNotFoundException(ClientNotFoundException ex) {
        return new ResponseError(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected ResponseError handleException(Exception ex) {
        return new ResponseError(ex.getMessage());
    }

    protected class ResponseError {
        private String detail;

        public ResponseError(String detail) {
            this.detail = detail;
        }

        public String getDetail() {
            return detail;
        }
    }
}
