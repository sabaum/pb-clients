package br.com.platformbuilders.client.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table
public final class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", updatable = false, nullable = false)
    private Long id;

    @Column
    private String name;

    @Column(name = "DOCUMENT_NUMBER", length = 11, nullable = false)
    private String documentNumber;

    @Column(name = "BIRTH_DATE", nullable = false)
    private LocalDate birthDate;

    Client() {}

    public static Client from(String name, String documentNumber) {
        Client client = new Client();
        client.name = name;
        client.documentNumber = documentNumber;
        return client;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
