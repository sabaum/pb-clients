package br.com.platformbuilders.client.domain;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    ClientDTO entityToDTO(Client request);

    Client requestToEntity(ClientCreationRequest client);
}
