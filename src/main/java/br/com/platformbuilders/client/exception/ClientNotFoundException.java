package br.com.platformbuilders.client.exception;

public class ClientNotFoundException extends RuntimeException {

    public ClientNotFoundException(Long clientId) {
        super(String.format("Client with id %d not found", clientId));
    }
}
