package br.com.platformbuilders.client.repository;

import br.com.platformbuilders.client.domain.Client;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {

    Page<Client> findAll(Example<Client> example, Pageable pageable);
}
