package br.com.platformbuilders.client.rest;

import br.com.platformbuilders.client.domain.ClientCreationRequest;
import br.com.platformbuilders.client.domain.ClientDTO;
import br.com.platformbuilders.client.domain.ClientUpdateRequest;
import br.com.platformbuilders.client.service.IClientService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(path = "/client")
public class ClientResource {

    private final IClientService clientService;

    public ClientResource(IClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<ClientDTO> find(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String documentNumber,
            Pageable pageable) {
        return clientService.find(name, documentNumber, pageable);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid ClientCreationRequest client) {
        clientService.create(client);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody @Valid ClientUpdateRequest client) {
        clientService.update(client);
    }

    @PatchMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public void updateInformation(@RequestBody ClientUpdateRequest client) {
        clientService.updateInformation(client);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") @NotNull Long clientId) {
        clientService.delete(clientId);
    }
}
