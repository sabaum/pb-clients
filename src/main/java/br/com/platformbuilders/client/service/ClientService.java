package br.com.platformbuilders.client.service;

import br.com.platformbuilders.client.domain.*;
import br.com.platformbuilders.client.exception.ClientNotFoundException;
import br.com.platformbuilders.client.repository.ClientRepository;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
class ClientService implements IClientService {

    private final ClientRepository repository;
    private final ClientMapper mapper;

    ClientService(ClientRepository repository, ClientMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Transactional
    public void create(ClientCreationRequest client) {
        repository.save(mapper.requestToEntity(client));
    }

    @Override
    public Page<ClientDTO> find(String name, String documentNumber, Pageable pageable) {
        Example<Client> client = Example.of(Client.from(name, documentNumber), ExampleMatcher.matchingAll().withIgnoreCase());
        Page<Client> pagedClient = repository.findAll(client, pageable);
        List<ClientDTO> clientDTOList = pagedClient.stream().map(mapper::entityToDTO).collect(Collectors.toList());

        return new PageImpl<>(clientDTOList, pageable, pagedClient.getTotalElements());
    }

    @Override
    @Transactional
    public void delete(Long clientId) {
        Client client = repository.findById(clientId).orElseThrow(() -> new ClientNotFoundException(clientId));
        repository.delete(client);
    }

    @Override
    @Transactional
    public void update(ClientUpdateRequest clientRequest) {
        Client client = repository.findById(clientRequest.getId()).orElseThrow(() -> new ClientNotFoundException(clientRequest.getId()));
        client.setBirthDate(clientRequest.getBirthDate());
        client.setDocumentNumber(clientRequest.getDocumentNumber());
        client.setName(clientRequest.getName());

        repository.save(client);
    }

    @Override
    @Transactional
    public void updateInformation(ClientUpdateRequest clientRequest) {
        Client client = repository.findById(clientRequest.getId()).orElseThrow(() -> new ClientNotFoundException(clientRequest.getId()));
        if (clientRequest.getName() != null) {
            client.setName(clientRequest.getName());
        }
        if (clientRequest.getDocumentNumber() != null) {
            client.setDocumentNumber(clientRequest.getDocumentNumber());
        }
        if (clientRequest.getBirthDate() != null) {
            client.setBirthDate(clientRequest.getBirthDate());
        }

        repository.save(client);
    }
}
