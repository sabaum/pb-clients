package br.com.platformbuilders.client.service;

import br.com.platformbuilders.client.domain.ClientDTO;
import br.com.platformbuilders.client.domain.ClientCreationRequest;
import br.com.platformbuilders.client.domain.ClientUpdateRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IClientService {
    void create(ClientCreationRequest client);

    Page<ClientDTO> find(String name, String documentNumber, Pageable pageable);

    void delete(Long clientId);

    void update(ClientUpdateRequest client);

    void updateInformation(ClientUpdateRequest client);
}
