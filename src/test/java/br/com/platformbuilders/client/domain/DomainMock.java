package br.com.platformbuilders.client.domain;

import java.time.LocalDate;

public class DomainMock {

    public static Client clientMock() {
        return new Client();
    }

    public static ClientCreationRequest clientCreationRequestMock() {
        return new ClientCreationRequest();
    }

    public static ClientUpdateRequest clientUpdateRequestMock() {
        ClientUpdateRequest client = new ClientUpdateRequest();
        client.setId(1L);
        client.setName("name");
        client.setDocumentNumber("12345678910");
        client.setBirthDate(LocalDate.now().minusYears(20));

        return client;
    }
}
