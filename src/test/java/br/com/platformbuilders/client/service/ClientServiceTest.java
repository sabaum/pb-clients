package br.com.platformbuilders.client.service;

import br.com.platformbuilders.client.domain.*;
import br.com.platformbuilders.client.exception.ClientNotFoundException;
import br.com.platformbuilders.client.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    @Mock
    private ClientRepository repository;
    @Mock
    private ClientMapper mapper;

    @InjectMocks
    private ClientService service;

    private final Client mockClient = DomainMock.clientMock();
    private final ClientCreationRequest mockCreationClient = DomainMock.clientCreationRequestMock();
    private final ClientUpdateRequest mockUpdateClient = DomainMock.clientUpdateRequestMock();
    private final Long clientId = 1L;
    private final String name = "anyName";
    private final String documentNumber = "12345678910";
    private final Pageable pageable = Pageable.unpaged();
    private final Page<Client> pagedCLients = new PageImpl<>(List.of(mockClient));

    @Test
    void create_shouldSaveClient() {
        when(mapper.requestToEntity(mockCreationClient)).thenReturn(mockClient);

        service.create(mockCreationClient);

        Mockito.verify(repository).save(any(Client.class));
    }

    @Test
    void find_shouldFindAll() {
        when(repository.findAll(any(Example.class), any(Pageable.class))).thenReturn(pagedCLients);

        service.find(name, documentNumber, pageable);

        verify(repository).findAll(any(Example.class), eq(pageable));
    }

    @Test
    void delete_whenClientFound_shouldDeleteIt() {
        when(repository.findById(clientId)).thenReturn(Optional.of(mockClient));

        service.delete(clientId);

        verify(repository).delete(mockClient);
    }

    @Test
    void delete_whenClientNotFound_shouldThrowClientNotFoundException() {
        when(repository.findById(clientId)).thenReturn(Optional.empty());

        assertThrows(ClientNotFoundException.class, () -> {
            service.delete(clientId);
        });

    }

    @Test
    void update_whenClientFound_shouldUpdateIt() {
        when(repository.findById(clientId)).thenReturn(Optional.of(mockClient));

        service.update(mockUpdateClient);

        verify(repository).save(mockClient);
    }

    @Test
    void update_whenClientNotFound_shouldThrowClientNotFoundException() {
        when(repository.findById(clientId)).thenReturn(Optional.empty());

        assertThrows(ClientNotFoundException.class, () -> {
            service.update(mockUpdateClient);
        });
    }

    @Test
    void update_shouldUpdateAllFields() {
        when(repository.findById(clientId)).thenReturn(Optional.of(mockClient));

        var newName = "newName";
        var newBirthDate = LocalDate.now();
        var newDocumentNumber = "11111111111";
        mockUpdateClient.setName(newName);
        mockUpdateClient.setBirthDate(newBirthDate);
        mockUpdateClient.setDocumentNumber(newDocumentNumber);
        service.update(mockUpdateClient);

        ArgumentCaptor<Client> argument = ArgumentCaptor.forClass(Client.class);

        verify(repository).save(argument.capture());
        assertEquals(newName, argument.getValue().getName());
        assertEquals(newBirthDate, argument.getValue().getBirthDate());
        assertEquals(newDocumentNumber, argument.getValue().getDocumentNumber());
    }

    @Test
    void updateInformationFound_shouldUpdateInformation() {
        when(repository.findById(clientId)).thenReturn(Optional.of(mockClient));

        service.updateInformation(mockUpdateClient);

        verify(repository).save(mockClient);
    }

    @Test
    void updateInformationNotFound_shouldThrowClientNotFoundException() {
        when(repository.findById(clientId)).thenReturn(Optional.empty());

        assertThrows(ClientNotFoundException.class, () -> {
            service.updateInformation(mockUpdateClient);
        });
    }

    @Test
    void updateInformation_whenSendingOnlyName_shouldUpdateOnlyName() {
        when(repository.findById(clientId)).thenReturn(Optional.of(mockClient));

        var newName = "newName";
        mockUpdateClient.setName(newName);
        mockUpdateClient.setBirthDate(null);
        mockUpdateClient.setDocumentNumber(null);

        service.updateInformation(mockUpdateClient);

        ArgumentCaptor<Client> argument = ArgumentCaptor.forClass(Client.class);

        verify(repository).save(argument.capture());
        assertEquals(newName, argument.getValue().getName());
        assertNull(argument.getValue().getBirthDate());
        assertNull(argument.getValue().getDocumentNumber());
    }

    @Test
    void updateInformation_whenSendingOnlyBirthDate_shouldUpdateOnlyBirthDate() {
        when(repository.findById(clientId)).thenReturn(Optional.of(mockClient));

        var birthDate = LocalDate.now();
        mockUpdateClient.setName(null);
        mockUpdateClient.setBirthDate(birthDate);
        mockUpdateClient.setDocumentNumber(null);

        service.updateInformation(mockUpdateClient);

        ArgumentCaptor<Client> argument = ArgumentCaptor.forClass(Client.class);

        verify(repository).save(argument.capture());
        assertEquals(birthDate, argument.getValue().getBirthDate());
        assertNull(argument.getValue().getName());
        assertNull(argument.getValue().getDocumentNumber());
    }

    @Test
    void updateInformation_whenSendingOnlyDocumentNumber_shouldUpdateOnlyDocumentNumber() {
        when(repository.findById(clientId)).thenReturn(Optional.of(mockClient));

        var documentNumber = "11111111111";
        mockUpdateClient.setName(null);
        mockUpdateClient.setBirthDate(null);
        mockUpdateClient.setDocumentNumber(documentNumber);

        service.updateInformation(mockUpdateClient);

        ArgumentCaptor<Client> argument = ArgumentCaptor.forClass(Client.class);

        verify(repository).save(argument.capture());
        assertEquals(documentNumber, argument.getValue().getDocumentNumber());
        assertNull(argument.getValue().getName());
        assertNull(argument.getValue().getBirthDate());
    }

}